var Promise = require('bluebird');
var unirest = require('unirest');
var myFirebaseRef = require('./SambungkatabotFirebase.js');

var MASHAPE_KEY = process.env.MASHAPE_KEY;

var dictionaryEnglish = (function () {
    'use strict';

    function getStartingWord(type) {
      return new Promise(function(resolve, reject) {
        unirest.get("https://wordsapiv1.p.mashape.com/words?letterPattern=^[A-Za-z]*$&partOfSpeech="+type.toLowerCase()+"&random=true")
            .header("X-Mashape-Key", MASHAPE_KEY)
            .header("Accept", "application/json")
            .end(function (result) {
                if (result.status === 200) {
                  resolve(result.body.word);
                } else {
                  reject({
                      code: 2,
                      message: "Word not found"
                  });
                }
            });
      });
    }

    function checkWord(word, type) {
      return new Promise(function(resolve, reject) {
        unirest.get("https://wordsapiv1.p.mashape.com/words/" + word)
            .header("X-Mashape-Key", MASHAPE_KEY)
            .header("Accept", "application/json")
            .end(function (result) {
                if (result.status !== 200) {
                    return reject({
                        code: 2,
                        message: "Word not found"
                    });
                } else {
                    let searchResult = result.body.results || [],
                        match = false;
                    for (var i = 0; i < searchResult.length && !match; i++) {
                        //  Checking type of word is match with the current category
                        if (searchResult[i].partOfSpeech === type.toLowerCase()) {
                            match = true;
                        }
                    }

                    if (match) {
                        let wordTail = '',
                            found = false;
                        for (var j = word.length - 1; j >= 0 && !found; j--) {
                            //  Search for word tail to be continued by others
                            if ('aioue'.indexOf(word.charAt(j)) == -1) {
                                wordTail = word.charAt(j);
                                found = true;
                            }
                        }
                        saveToLocal(word, type);
                        return resolve(wordTail);
                    } else {
                        return reject({
                            code: 2,
                            message: "Word not found"
                        });
                    }
                }
            });
      });
    }

    function englishCheckWord(word, type) {
      return searchLocal(word, type).catch(function(error) {
        return checkWord(word, type);
      });
    }

    function saveToLocal(word, category) {
      return myFirebaseRef.child('bank').child('english').child(category).child(word).transaction(function(oldData) {
        return true;
      }).then(function(saveTx) {
        if(saveTx.committed){
          return Promise.resolve();
        } else {
          return Promise.reject();
        }
      })
    }

    function searchLocal(word, category) {
      return myFirebaseRef.child('bank').child('english').child(category).child(word).once('value').then(function(wordSnapshot) {
        if(wordSnapshot.exist()){
          var wordTail = '',
              found = false;
          for (var j = word.length - 1; j >= 0 && !found; j--) {
              //  Search for word tail to be continued by others
              if ('aioue'.indexOf(word.charAt(j)) == -1) {
                  wordTail = word.charAt(j);
                  found = true;
              }
          };
          return Promise.resolve(wordTail);
        } else {
          return Promise.reject();
        }
      })
    }

    return {
        getStartingWord: getStartingWord,
        checkWord: englishCheckWord
    }
}());

module.exports = dictionaryEnglish;
