if (process.env.NEW_RELIC_LISENCE_KEY) {
    var newrelic = require('newrelic');
}

var TelegramBot = require('node-telegram-bot-api');
var express = require('express');

var app = express();

// var token = '180681965:AAHtBGEapx0vikKqu4Z0QTSqzDHZOD9ctYk';
var token = process.env.TELEGRAM_BOT_TOKEN || '180681965:AAHtBGEapx0vikKqu4Z0QTSqzDHZOD9ctYk';
var servername = process.env.TELEGRAM_BOT_SERVER_NAME || 'localhost:8080';

// Setup polling way
var bot = new TelegramBot(token, {
    polling: true
});

var processor = require('./Processor.js')(bot);


bot.onText(/\/startgame/, function responseStartMessage(message) {
    processor.incomingMessageProcessor(message);
    processor.startGameProcessor(message);
});

bot.onText(/\/join/, function responseStartMessage(message) {
    processor.incomingMessageProcessor(message);
    processor.joinProcessor(message);
});

bot.onText(/\/buatgame/, function buatGameResponse(message) {
    processor.incomingMessageProcessor(message);
    processor.buatGameProcessor(message);
});

bot.onText(/\/help/, function responseHelp(message) {
    processor.incomingMessageProcessor(message);
    processor.helpProcessor(message);
});

bot.onText(/\/jawab/, function responseStartMessage(message) {
    processor.incomingMessageProcessor(message);
    processor.jawabProcessor(message);
});

bot.on('inline_query', function (inlineQuery) {
    processor.inlineQueryProcessor(inlineQuery);
});

bot.on('new_chat_participant', function (message) {
    var copyMessage = message;
    copyMessage.text = 'Invited';
    processor.incomingMessageProcessor(copyMessage);
    processor.newChatParticipantProcessor(message);
});

bot.on('left_chat_participant', function (message) {
  if(message.left_chat_member.username == process.env.TELEGRAM_BOT_USERNAME){
    var copyMessage = message;
    copyMessage.text = 'Deleted';
    processor.incomingMessageProcessor(copyMessage);
    processor.deleteRoomProcessor(message.chat.id);
  }
});

bot.onText(/\/setshufflemoderandom/, function setRandomMode(message) {
    processor.incomingMessageProcessor(message);
    processor.randomModeProcessor(message.chat.id, 'Random');
});

bot.onText(/\/setshufflemodenormal/, function setRandomMode(message) {
    processor.incomingMessageProcessor(message);
    processor.randomModeProcessor(message.chat.id, 'Normal');
});

bot.onText(/\/alive/, function setRandomMode(message) {
    processor.incomingMessageProcessor(message);
    processor.isAliveProcessor(message);
});

bot.onText(/\/howto/, function howToInstruction(message) {
  processor.incomingMessageProcessor(message);
  processor.howToProcessor(message);
});

bot.onText(/\/setgamemodeindonesia/, function howToInstruction(message) {
  processor.incomingMessageProcessor(message);
  processor.gameModeProcessor(message.chat.id, 'Indonesia');
});

bot.onText(/\/setgamemodeverb/, function howToInstruction(message) {
  processor.incomingMessageProcessor(message);
  processor.gameModeProcessor(message.chat.id, 'Verb');
});

bot.onText(/\/setgamemodenoun/, function howToInstruction(message) {
  processor.incomingMessageProcessor(message);
  processor.gameModeProcessor(message.chat.id, 'Noun');
});

bot.onText(/\/setgamemodeadjective/, function howToInstruction(message) {
  processor.incomingMessageProcessor(message);
  processor.gameModeProcessor(message.chat.id, 'Adjective');
});

bot.onText(/\/showleaderboard/, function showHighScore(message) {
  processor.incomingMessageProcessor(message);
  processor.showHighScoreProcessor(message);
})

// bot.on('message', function allMessage(message) {
//   console.log("response message", message);
// });

app.get('/', function (request, response) {
    response.send('hello');
});

app.get('/fb-webhook/', function (req, res) {
    if (req.query['hub.verify_token'] === 'sambungkatabot_facebook_messenger') {
        res.send(req.query['hub.challenge']);
    }
    res.send('Error, wrong validation token');
});

app.post('/fb-webhook/', function (req, res) {
  console.log(req.body);
    messaging_events = req.body.entry[0].messaging;
    for (i = 0; i < messaging_events.length; i++) {
        event = req.body.entry[0].messaging[i];
        sender = event.sender.id;
        if (event.message && event.message.text) {
            text = event.message.text;
            console.log("Facebook Messenger", text);
        }
    }
    res.sendStatus(200);
});

var server;

processor.resetData().then(function () {
    server = app.listen(process.env.PORT || 8080, function () {
        console.log("Sambungkatabot is up!");
    });
});
