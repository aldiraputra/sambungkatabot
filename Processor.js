var RateLimiter = require('limiter').RateLimiter;
const _ = require('underscore');
// Allow 150 requests per hour (the Twitter search limit). Also understands
// 'second', 'minute', 'day', or a number of milliseconds
var limiter = new RateLimiter(10, 'second');
var random = require('random-js')();
var Promise = require('bluebird');
var shuffle = require('knuth-shuffle').knuthShuffle;
var i18next = require('i18next');
var Backend = require('i18next-node-fs-backend');


i18next.use(Backend)
  .init({
    "lng": "en",
    "fallbackLng": "id",
    "defaultNS": "common",
    "ns": "common",
    "backend": {
      "loadPath": 'locale/{{lng}}/{{ns}}.json'
    }
  });

// var firebase = Promise.promisifyAll(require('./SambungkatabotFirebase.js'));
var osmosis = Promise.promisifyAll(require('osmosis'));

var myFirebaseRef = require('./SambungkatabotFirebase.js');
var indoDict = require('./DictionaryIndonesia.js');
var engDict = require('./DictionaryEnglish.js');
var chatBottleHandler = require('./ChatBottleHandler.js');
var botanToken = process.env.BOTAN_TOKEN;
var botan = require('botanio')(botanToken);
var botUsername = process.env.TELEGRAM_BOT_USERNAME || 'diratest_bot';

//  Catch all uncaught exception
process.on("unhandledRejection", function(reason, promise) {
  console.log('GLOBAL ERROR', reason);
});

var processor = function(bot) {
  'use strict';
  var timeoutSec = 40;
  var buatTimeoutMin = 5;
  var sendDelay = 4 * 1000;
  var timeout = 1000 * (timeoutSec);
  var buatTimeout = 1000 * 60 * buatTimeoutMin;
  var counter = {};
  var counterTime = {};
  var counterBuat = {};
  var busy = {};
  var busyStart = {};
  var busyBuat = {};
  var busyTimeout = {};
  var gameRecord = {};
  var SKIP_ERROR_MESSAGE = "SKIP_ERROR_MESSAGE";
  var roomLimiter = {};
  var roomLanguage = {};
  var usedWord = {};

  function sendMessage(chatId, content, options) {
    return Promise.delay(sendDelay).then(function() {
      new Promise(function(resolve, reject) {
        limiter.removeTokens(1, function() {
          if (roomLimiter[chatId] === undefined) {
            roomLimiter[chatId] = new RateLimiter(10, 'minute');
          };
          roomLimiter[chatId].removeTokens(1, function() {
            bot.sendChatAction(chatId, 'typing');
            bot.sendMessage(chatId, content, options).then(function(msg) {
              chatBottleHandler.outgoingMessageProcessor(msg.message_id, msg.text, msg.chat.id)
              resolve(msg);
            }).catch(function(error) {
              console.log("Send Message error", chatId, error);
              var messageJson = error.message,
                errorDetails = messageJson.substring(messageJson.indexOf('{'), messageJson.length),
                errorObject = JSON.parse(errorDetails);
              if (errorObject.error_code === 400 || errorObject.error_code === 403) {
                myFirebaseRef.child("rooms").child(chatId).remove();
              }
            });
          });
        });
      });
    });
  };

  function isGameEnd(gameId) {
    return new Promise(function(resolve, reject) {
      if(_.keys(usedWord[gameId]).length == 0){
        console.log('length', usedWord[gameId]);
        reject({
          code: 8,
          message: "Game has ended"
        });
      } else {
          myFirebaseRef.child("games").child(gameId).child("player").once('value').then(function(gameSnapshot) {
            myFirebaseRef.child('games').child(gameId).child('type').once('value').then(function gameTypeSnapshot(typeSnapshot) {
              if (typeSnapshot.val() == 'private') {
                if (gameSnapshot.numChildren() == 0) {
                  reject({
                    code: 8,
                    message: "Game has ended"
                  });
                } else {
                  resolve();
                }
              } else {
                if (gameSnapshot.numChildren() == 1) {
                  reject({
                    code: 8,
                    message: "Game has ended"
                  });
                } else {
                  resolve();
                }
              }
            });
          });
      }
    });
  }

  function isAlive(gameid, playerId) {
    return new Promise(function(resolve, reject) {
      myFirebaseRef.child("games").child(gameid).child("player").child(playerId).once('value').then(function(playerSnapshot) {
        if (playerSnapshot.val() == null) {
          reject({
            code: 9,
            message: "Player is not registered"
          });
        } else {
          resolve();
        }
      });
    });
  };

  function saveRecord(chatId, playerData, total) {
    return myFirebaseRef.child('rooms').child(chatId).child('scores').child(playerData.id).transaction(function updateScores(oldData) {
      var newScores = playerData;
      // Append attribute score to player data
      newScores['score'] = total;
      if (oldData == null) {
        return newScores;
      } else if (oldData.score < newScores.score) {
        return newScores;
      } else {
        return oldData;
      }
    }).then(function(scoreTrx) {
      if (scoreTrx.committed) {
        return Promise.resolve();
      }
    });
  }

  var isAliveProcessor = function(message) {
    var currentGameId;
    return getGameId(message.chat.id).then(function(gameId) {
      currentGameId = gameId;
      return isGameNotStarted(gameId);
    }).catch(function(error) {
      // console.log("alive error", error);
      if (error.code == 7) {
        isAlive(currentGameId, message.from.id).then(function() {

          sendMessage(message.chat.id, i18next.t('{{username}} is alive', {
            lng: getLanguage(message.chat.id),
            replace: {
              username: (message.from.username) ? '@' + message.from.username : message.from.first_name + " " + message.from.last_name
            }
          }));
        }).catch(function(error) {
          console.log("is alive error", error);
          if (error.code == 9) {
            sendMessage(message.chat.id, i18next.t('{{username}} is out', {
              lng: getLanguage(message.chat.id),
              replace: {
                username: (message.from.username) ? '@' + message.from.username : message.from.first_name + " " + message.from.last_name
              }
            }));

          };
        });
      }
    })
  };

  function endGame(chatId) {
    var currentGameId,
      totalWordCount;
    getGameId(chatId).then(function(gameId) {
      currentGameId = gameId;
      //  Disable the game
      return myFirebaseRef.child("ingame").child(chatId).transaction(function(data) {
        return false;
      }).then(function(commited, snapshot) {
        return new Promise(function(resolve, reject) {
          resolve();
        });
      });
    }).then(function() {
      return getTotalWordCount(currentGameId);
    }).then(function(totalWord) {
      totalWordCount = totalWord;
      //  Get The winner player id
      return myFirebaseRef.child("games").child(currentGameId).once('value');
    }).then(function(snapshot) {
      //  Get the winner player data
      return myFirebaseRef.child("games").child(currentGameId).child("player").once('value');
    }).then(function(winnerSnapshot) {
      // Get Winner details for leaderboard
      winnerSnapshot.forEach(function winnerData(winInfo) {
        myFirebaseRef.child("games").child(currentGameId).child("player").child(winInfo.key).once('value').then(function getWinnerInfo(winnerInfo) {
          var playerOut = (winnerInfo.val().username) ? '@' + winnerInfo.val().username : winnerInfo.val().first_name + " " + winnerInfo.val().last_name;
          saveRecord(chatId, winnerInfo.exportVal(), gameRecord[chatId][playerOut] || 0);
        });
      });

      updateStreak(chatId, totalWordCount).then(function(maxStreak) {
        var newRecord = (maxStreak == totalWordCount) ? i18next.t('new record {{max}}', {
            lng: getLanguage(chatId),
            replace: {
              max: maxStreak
            }
          }) : "",
          gameRecordString = ["Player Scores :\n"],
          max = -1,
          maxPlayer = "",
          mvpString = "\nMVP of the game : ",
          showLeaderboardString = i18next.t('show leaderboard', {
            lng: getLanguage(chatId)
          });

        for (var player in gameRecord[chatId]) {
          if (gameRecord[chatId].hasOwnProperty(player)) {
            if (max < gameRecord[chatId][player]) {
              max = gameRecord[chatId][player];
              maxPlayer = player;
            }
            gameRecordString.push(i18next.t('{{player}} score {{total}}', {
              lng: getLanguage(chatId),
              replace: {
                player: player,
                total: gameRecord[chatId][player]
              }
            }));
          }
        }

        myFirebaseRef.child("games").child(currentGameId).child("type").once('value').then(function(gameTypeSnapshot) {
          if (gameTypeSnapshot.val() == 'private') {
            sendMessage(chatId, i18next.t('game end {{total}}', {
              lng: getLanguage(chatId),
              replace: {
                total: totalWordCount
              }
            }) + newRecord, {
              parse_mode: 'HTML'
            }).then(function() {
              busyTimeout[chatId] = false;
            });
          } else {
            winnerSnapshot.forEach(function(winner) {
              sendMessage(chatId, i18next.t('game end {{winner}} {{total}}', {
                lng: getLanguage(chatId),
                replace: {
                  winner: (winner.val().username) ? '@' + winner.val().username : winner.val().first_name + " " + winner.val().last_name,
                  total: totalWordCount
                }
              }) + newRecord + "\n\n" + mvpString + maxPlayer + "\n\n" + gameRecordString.join("") + showLeaderboardString + "\nRate Sambungkata  : https://telegram.me/storebot?start=sambungkatabot", {
                parse_mode: 'HTML'
              }).then(function() {
                busyTimeout[chatId] = false;
              });
            });
          }
        });
      });
      busyBuat[chatId] = false;
      busyStart[chatId] = false;
      busy[chatId] = false;
      usedWord[currentGameId] = undefined;
      counterTime[chatId] = 0;
    });
  };

  function updateStreak(chatId, currentStreak) {
    return new Promise(function(resolve, reject) {
      myFirebaseRef.child("rooms").child(chatId).child("longest").transaction(function(data) {
        if (data == null) {
          return currentStreak;
        } else {
          return (data < currentStreak) ? currentStreak : data;
        }
      }).then(function(longestTx) {
        if (longestTx.committed) {
          resolve(longestTx.snapshot.val())
        }
      });
    });
  };

  function timeoutBuatAction(chatId) {
    myFirebaseRef.child("rooms").child(chatId).child("game").transaction(function(data) {
      return false;
    }).then(function(removeTx) {
      return new Promise(function(resolve, reject) {
        if (removeTx.committed) {
          sendMessage(chatId, i18next.t('not enough player', {
            lng: getLanguage(chatId)
          }));
          busyBuat[chatId] = false;
          resolve();
        } else {
          reject();
        }
      });
    });
  };

  //  Action that triggered when the turn is over
  function timeoutAction(chatId, sylabel) {
    var currentGameId,
      playerIdOut,
      playerOut;
    busyTimeout[chatId] = true;
    bot.sendChatAction(chatId, 'typing');
    getGameId(chatId).then(function(gameId) {
      currentGameId = gameId;
      return getGameObject(currentGameId);
    }).then(function(gameObject) {
      playerIdOut = gameObject.nextPlayer.split(',').slice(-1)[0];
      return myFirebaseRef.child("games").child(currentGameId).child("player").child(playerIdOut).once('value');
    }).then(function(playerSnapshot) {
      playerOut = (playerSnapshot.val().username) ? '@' + playerSnapshot.val().username : playerSnapshot.val().first_name + " " + playerSnapshot.val().last_name;
      gameRecord[chatId][playerOut] = gameRecord[chatId][playerOut] || 0;
      saveRecord(chatId, playerSnapshot.exportVal(), gameRecord[chatId][playerOut]);
      return removePlayer(currentGameId, playerIdOut);
    }).then(function() {
      return isGameEnd(currentGameId);
    }).then(function() {
      return getNextPlayer(chatId);
    }).then(function(username) {
      return sendMessage(chatId, i18next.t('{{player}} out of the game', {
        lng: getLanguage(chatId),
        replace: {
          player: playerOut
        }
      }) + i18next.t('{{player}} answer for {{sylabel}} in {{timeout}}', {
        lng: getLanguage(chatId),
        replace: {
          player: username,
          sylabel: sylabel,
          timeout: timeoutSec
        }
      }), {
        parse_mode: 'HTML'
      });
    }).then(function() {
      busy[chatId] = false;
      busyTimeout[chatId] = false;
      counterTime[chatId.toString()] = Date.now();
      counter['time' + chatId.toString()] = setTimeout(timeoutAction.bind(null, chatId, sylabel), timeout);
    }).catch(function(error) {
      // console.log("timeout catch", error);
      //  game end
      if (error.code == 8) {
        sendMessage(chatId, i18next.t('{{player}} out of the game', {
          lng: getLanguage(chatId),
          replace: {
            player: playerOut
          }
        })).then(function() {
          endGame(chatId);
        });
      } else {
        busyTimeout[chatId] = false;
      }
    });
  };

  function initGame(chatid, words, sylabels) {
    var currentGameId;
    gameRecord[chatid] = {};

    return getGameId(chatid).then(function(gameId) {
      currentGameId = gameId;
      console.log('remove used word', usedWord[gameId]);
      usedWord[gameId] = {};
      return myFirebaseRef.child("rooms").child(chatid).transaction(function(data) {
        if (data === null) {
          return;
        }
        data.game = gameId
        return data;
      }).then(function(commited, boolean) {
        return new Promise(function(resolve, reject) {
          resolve();
        });
      });
    }).then(function() {
      return addUsedWord(currentGameId, words);
    }).then(function() {
      return updateSylabel(currentGameId, sylabels);
    }).then(function() {
      return setRandomPlayerOnGame(chatid);
    }).then(function() {
      // console.log('after set player');
      return new Promise(function(resolve, reject) {
        resolve(currentGameId);
      });
    });
  };

  function getAllPlayer(gameId) {
    return new Promise(function(resolve, reject) {
      var playerIds = [];
      myFirebaseRef.child("games").child(gameId).child("player").once("value").then(function(snapshot) {
        snapshot.forEach(function(playerSnapshot) {
          playerIds.push(playerSnapshot.key);
        });
        resolve(playerIds);
      });
    });
  };

  function setRandomPlayerOnGame(chatid) {
    var randomMode,
      currentGameId;
    return getRandomMode(chatid).then(function(random) {
      return new Promise(function(resolve, reject) {
        if (random == 'Normal') {
          // console.log('Normal order player');
          getGameId(chatid).then(function(gameid) {
            currentGameId = gameid;
            return getAllPlayer(gameid);
          }).then(function(players) {
            myFirebaseRef.child("games").child(currentGameId).child("nextPlayer").transaction(function(data) {
              return shuffle(players).join(',');
            }).then(function(playerOrder) {
              if (playerOrder.committed) {
                resolve();
              }
            });
          });
        } else {
          // console.log('Random Player');
          resolve();
        }
      });
    });
  };

  function getRandomMode(chatid) {
    return new Promise(function(resolve, reject) {
      myFirebaseRef.child("rooms").child(chatid).child("random").once("value").then(function(snapshot) {
        if (snapshot.val() == null) {
          resolve('Random');
        } else {
          resolve(snapshot.val());
        }
      });
    });
  }

  function getGameMode(chatid) {
    return new Promise(function(resolve, reject) {
      myFirebaseRef.child("rooms").child(chatid).child("mode").once("value").then(function(snapshot) {
        if (snapshot.val() == null) {
          resolve('Indonesia');
        } else {
          resolve(snapshot.val());
        }
      });
    });
  }

  function getGameId(chatid) {
    return myFirebaseRef.child("ingame").child(chatid).once('value').then(function(snapshot) {
      return new Promise(function(resolve, reject) {
        if (snapshot.val() && snapshot.val() != false) {
          resolve(snapshot.val());
        } else {
          reject({
            code: 4,
            message: "no game in the room"
          });
        }
      });
    });
  };

  function getGameObject(gameId) {
    return myFirebaseRef.child("games").child(gameId).once('value').then(function(gameSnapshot) {
      return gameSnapshot.val();
    });
  };

  function getTotalWordCount(gameId) {
    return new Promise(function(resolve, reject) {
      resolve(_.keys(usedWord[gameId]).length)
    });
  };

  //  A promise for checking the word is use sylabel on starting word
  function checkUseSylabel(sylabel, word) {
    return new Promise(function(resolve, reject) {
      var subSylabels = [],
        found;
      if (sylabel.length <= 2) {
        subSylabels.push(sylabel);
      } else {
        for (var i = 0; i < sylabel.replace(/\r/g, "").replace(/\n/g, "").trim().length - 1; i++) {
          subSylabels.push(sylabel.trim().substring(i, sylabel.trim().length));
        }
      }
      for (var j = 0; j < subSylabels.length && !found; j++) {
        if (word.startsWith(subSylabels[j])) {
          found = true;
          resolve(word);
        }
      }
      if (!found) {
        reject({
          code: 0,
          message: "Sylabel missmatch " + sylabel + " against " + word
        });
      }
    });
  };

  function registerRoom(chat) {
    var roomData = {
      name : _.uniqueId('room_'),
      game : false
    };
    if (chat.type === 'private' && chat.username) {
      roomData.name = chat.username
    } else if (chat.type === 'group' && chat.title) {
      roomData.name = chat.title
    };
    roomData.name = roomData.name.replace(/[\#\.\$\[\]]+/g, '-');
    return new Promise(function(resolve, reject) {
      var roomRef = myFirebaseRef.child("rooms").child(chat.id);
      roomRef.once('value').then(function(snapshot) {
        if (snapshot.val() == null) {
          roomRef.set(roomData).then(resolve);
        } else {
          resolve();
        }
      });
    });
  };

  //  Check the word inputed is new (not repeat the word)
  function checkUsedWord(gameId, word) {
    return new Promise(function(resolve, reject) {
      if(_.has(usedWord[gameId], word)){
        reject({
          code: 1,
          message: "Word is used"
        });
      } else {
        resolve(word);
      }
    });
  };

  //  Add a word to the list to prevent use the same word.
  function addUsedWord(gameId, word) {
    usedWord[gameId][word] = true;
    return Promise.resolve();
  };

  function checkMinimumPlayer(chatId) {
    var currentGameId;
    return getGameId(chatId).then(function(gameId) {
      currentGameId = gameId;
      return myFirebaseRef.child("games").child(gameId).child("player").once('value');
    }).then(function(playerSnapshot) {
      return new Promise(function(resolve, reject) {
        myFirebaseRef.child("games").child(currentGameId).child("type").once('value').then(function(typeSnapshot) {
          if (playerSnapshot.numChildren() < 2 && typeSnapshot.val() !== 'private') {
            reject({
              code: 6,
              message: "Need more player"
            });
          } else {
            resolve();
          }
        });
      });
    }).then(function() {
      return isGameNotStarted(currentGameId);
    });
  };

  function isGameNotStarted(gameId) {
    return new Promise(function(resolve, reject) {
      if (_.keys(usedWord[gameId]).length == 0) {
        resolve();
      } else {
        reject({
          code: 7,
          message: "Game is already running"
        })
      }
    });
  };

  function getStartingWord(chatId) {
    return getGameMode(chatId).then(function(mode) {
      if (mode === 'Indonesia') {
        return indoDict.getStartingWord();
      } else {
        return engDict.getStartingWord(mode);
      }
    })
  };

  function addParticipant(gameId, player) {
    return isGameNotStarted(gameId).then(function() {
      var playerRef = myFirebaseRef.child("games").child(gameId).child("player").child(player.id);
      return playerRef.once('value')
        .then(function(snapshot) {
          return new Promise(function(resolve, reject) {
            if (snapshot.val() == null) {
              playerRef.set(player).then(function() {
                resolve(false);
              });
            } else {
              resolve(true);
            }
          });
        });
    });
  };

  function removePlayer(gameId, playerId) {
    var nextPlayerRef = myFirebaseRef.child("games").child(gameId).child("nextPlayer");
    return nextPlayerRef.once("value").then(function(nextPlayerSnapshot) {
      var gameData = nextPlayerSnapshot.val(),
        oldPlayerOrder = gameData.split(','),
        newPlayerOrder;
      if (oldPlayerOrder.length > 1) {
        newPlayerOrder = oldPlayerOrder.slice(0, oldPlayerOrder.length - 1);
      } else {
        newPlayerOrder = oldPlayerOrder.slice(0);
      }
      return nextPlayerRef.transaction(function(data) {
        return newPlayerOrder.join(',');
      });
    }).then(function(removeTx) {
      if (removeTx.committed) {
        return myFirebaseRef.child("games").child(gameId).child("player").child(playerId).remove();
      }
    });
  };

  function getNextPlayer(chatid) {
    var currentGameId;
    return getGameId(chatid).then(function(gameId) {
      currentGameId = gameId;
      return getRandomMode(chatid)
    }).then(function(randomMode) {
      // console.log("get next player random mode", randomMode);
      if (randomMode == 'Random') {
        return randomNextPlayer(currentGameId)
      } else if (randomMode == 'Normal') {
        // console.log("on normal mode");
        return normalNextPlayer(currentGameId);
      }
    });
  };

  function normalNextPlayer(gameId) {
    var gameRef = myFirebaseRef.child("games").child(gameId),
      nextPlayerRef = gameRef.child("nextPlayer");
    return new Promise(function(resolve, reject) {
      nextPlayerRef.once("value").then(function(playerOrderSnapshot) {
        var playerIds = playerOrderSnapshot.val().split(',').map(function(id) {
            return id.trim();
          }),
          nextPlayerId = playerIds[0],
          newPlayerOrder = playerIds.slice(1);

        newPlayerOrder.push(nextPlayerId);

        //  Push new Order to the data
        nextPlayerRef.transaction(function(data) {
          return newPlayerOrder.join(',');
        }).then(function(newPlayerOrderTx) {
          if (newPlayerOrderTx.committed) {
            //  Get Next Player Username
            gameRef.child("player").child(nextPlayerId).once('value').then(function(playerSnapshot) {
              resolve((playerSnapshot.val().username) ? '@' + playerSnapshot.val().username : playerSnapshot.val().first_name + " " + playerSnapshot.val().last_name);
            });
          }
        });
      });
    });
  };

  function randomNextPlayer(gameId) {
    var gameRef = myFirebaseRef.child("games").child(gameId);
    return gameRef.child("player").once('value').then(function(snapshot) {
      return new Promise(function(resolve, reject) {
        var nextId = Math.floor(Math.random() * (snapshot.numChildren())),
          iterate = 0,
          nextPlayerId, nextPlayerUsername;
        snapshot.forEach(function(playerSnapshot) {
          if (iterate === nextId) {
            nextPlayerId = playerSnapshot.key;
            nextPlayerUsername = (playerSnapshot.val().username) ? '@' + playerSnapshot.val().username : playerSnapshot.val().first_name + " " + playerSnapshot.val().last_name;
            return true;
          } else {
            iterate++;
          }
        });

        gameRef.child("nextPlayer").transaction(function(data) {
          return nextPlayerId;
        }).then(function(result) {
          if (result.committed) {
            resolve(nextPlayerUsername);
          };
        }).catch(function functionName(error) {
          reject({
            code: 5,
            message: "Invalid random function"
          });
        });
      });
    });
  }

  function checkWord(chatId, word) {
    return getGameMode(chatId).then(function(mode) {
      if (mode === 'Indonesia') {
        return indoDict.checkWord(word);
      } else {
        return engDict.checkWord(word, mode);
      }
    });
  };

  //  Update sylabel for the game
  function updateSylabel(gameId, sylabel) {
    return new Promise(function(resolve, reject) {
      return myFirebaseRef.child("games").child(gameId).child("latestSylabel").transaction(function(data) {
        return sylabel.trim();
      }).then(function(result) {
        if (result.committed) {
          resolve();
        }
      });
    });
  };

  function checkPlayer(gameId, gameObject, playerId) {
    return new Promise(function(resolve, reject) {
      if (gameObject.nextPlayer.endsWith(playerId)) {
        myFirebaseRef.child("games").child(gameId).child("player").child(playerId).once('value').then(function(playerSnapshot) {
          resolve({
            syl: gameObject.latestSylabel,
            username: (playerSnapshot.val().username) ? '@' + playerSnapshot.val().username : playerSnapshot.val().first_name + " " + playerSnapshot.val().last_name
          });
        });
      } else {
        reject({
          code: 3,
          message: "Invalid Player"
        });
      };
    });
  };

  function createGame(chatId, from, gameType) {
    var gamesRef = myFirebaseRef.child("games"),
      newGameRef;
    newGameRef = gamesRef.push({
      type: gameType
    });

    return newGameRef.then(function() {
      return newGameRef.child("player").child(from.id).set(from);
    }).then(function() {
      return myFirebaseRef.child("ingame").child(chatId).once('value');
    }).then(function(snapshot) {
      return myFirebaseRef.child("ingame").child(chatId).transaction(function(data) {
        return newGameRef.key;
      });
    }).then(function(result) {
      return new Promise(function(resolve, reject) {
        if (result.committed) {
          resolve();
        } else {
          reject();
        }
      });
    });
  };

  function processAnswer(chatId, fromId, answerString) {
    var gameId, currentSylabel, newSylabel,
      answer = answerString.toString().toLowerCase(),
      timeLeft = timeout - (Date.now() - counterTime[chatId.toString()]),
      currentPlayer = "";

    if (busyTimeout[chatId]) {
      console.log('Timeout process for', chatId);
      busy[chatId] = false;
      return;
    }

    clearTimeout(counter['time' + chatId.toString()]);
    // counterTime[chatId.toString()] = Date.now();
    bot.sendChatAction(chatId, 'typing');

    return getGameId(chatId).then(function(id) {
      //  Get game object
      gameId = id;
      return isGameEnd(id);
    }).then(function() {
      return getGameObject(gameId);
    }).then(function(gameObject) {
      return checkPlayer(gameId, gameObject, fromId);
    }).then(function(sylabel) {
      currentSylabel = sylabel.syl;
      currentPlayer = sylabel.username;
      return checkUseSylabel(sylabel.syl, answer);
    }).then(function(word) {
      return checkUsedWord(gameId, word);
    }).then(function(word) {
      return checkWord(chatId, word);
    }).then(function(sylabel) {
      newSylabel = sylabel;
      return isGameEnd(gameId)
    }).then(function() {
      return addUsedWord(gameId, answer);
    }).then(function() {
      return getNextPlayer(chatId).then(function(username) {
        bot.sendChatAction(chatId, 'typing');
        return Promise.delay(0).then(function() {
          return updateSylabel(gameId, newSylabel).then(function() {
            // clearTimeout(counter['time' + chatId.toString()]);
            return sendMessage(chatId, i18next.t('good', {
              lng: getLanguage(chatId)
            }) + i18next.t('{{player}} answer for {{sylabel}} in {{timeout}}', {
              lng: getLanguage(chatId),
              replace: {
                player: username,
                sylabel: newSylabel,
                timeout: timeoutSec
              }
            }), {
              parse_mode: 'HTML'
            });
          }).then(function() {
            gameRecord[chatId][currentPlayer] = gameRecord[chatId][currentPlayer] + 1 || 1;
            busy[chatId] = false;
            counterTime[chatId.toString()] = Date.now();
            counter['time' + chatId.toString()] = setTimeout(timeoutAction.bind(null, chatId, newSylabel), timeout);
          });
        });
      });
    }).catch(function(reason) {
      switch (reason.code) {
        case 0:
          sendMessage(chatId, i18next.t('{{word}} not match with sylabel', {
            lng: getLanguage(chatId),
            replace: {
              word: answer
            }
          })).then(function() {
            counter['time' + chatId.toString()] = setTimeout(timeoutAction.bind(null, chatId, currentSylabel), Math.max(timeLeft, 0));
            busy[chatId] = false;
          });
          break;
        case 1:
          sendMessage(chatId, i18next.t('{{word}} has been used', {
            lng: getLanguage(chatId),
            replace: {
              word: answer
            }
          })).then(function() {
            counter['time' + chatId.toString()] = setTimeout(timeoutAction.bind(null, chatId, currentSylabel), Math.max(timeLeft, 0));
            busy[chatId] = false;
          });
          break;
        case 2:
          sendMessage(chatId, i18next.t('{{word}} not found', {
            lng: getLanguage(chatId),
            replace: {
              word: answer
            }
          })).then(function() {
            counter['time' + chatId.toString()] = setTimeout(timeoutAction.bind(null, chatId, currentSylabel), Math.max(timeLeft, 0));
            busy[chatId] = false;
          });
          break;
        default:
          busy[chatId] = false;
      }
      busy[chatId] = false;
      console.log(reason);
    });
  };

  //  Concrete function for telegram bot
  var buatGameProcessor = function(message) {

    // if (!message.from.username) {
    //     sendMessage(message.chat.id, i18next.t('fail to {{command}} {{player}} not having username', {
    //         lng: getLanguage(message.chat.id),
    //         replace: {
    //             command: 'buatgame',
    //             player: message.from.first_name
    //         }
    //     }));
    //     return;
    // };
    var gamesRef = myFirebaseRef.child("games"),
      newGameRef,
      roomRef = myFirebaseRef.child("rooms").child(message.chat.id);
    if (busyBuat[message.chat.id]) {
      console.log('Buat game is busy for', message.chat.id);
      return;
    }
    busyBuat[message.chat.id] = true;
    registerRoom(message.chat).then(function() {
      return getGameId(message.chat.id);
    }).then(function(gameId) {
      return isGameNotStarted(gameId);
    }).then(function() {
      return sendMessage(message.chat.id, i18next.t('game already created', {
        lng: getLanguage(message.chat.id)
      }));
    }).catch(function(error) {
      console.log('Buat game error [' + message.chat.id + ']', error);
      if (error.code === 4) {
        createGame(message.chat.id, message.from, message.chat.type).then(function(msg) {
          counterBuat[message.chat.id] = setTimeout(timeoutBuatAction.bind(null, message.chat.id), buatTimeout);
          botan.track(message, "buatgame");
          return sendMessage(message.chat.id, i18next.t('game created with {{timeout}}', {
            lng: getLanguage(message.chat.id),
            replace: {
              timeout: buatTimeoutMin
            }
          }));
        });
      } else if (error.code === 7) {
        console.log(SKIP_ERROR_MESSAGE, error);
        // sendMessage(message.chat.id, "Ouch! Game sudah dimulai. Tunggu game selanjutnya untuk /buatgame");
      }
      busyStart[message.chat.id] = false;
      busyBuat[message.chat.id] = false;
    });
  };

  var jawabProcessor = function(message) {

    var messageText = message.text,
      answer = messageText.substring(messageText.indexOf(" ") + 1, messageText.length),
      gameId;
    //  Currently processing the answer
    if (busy[message.chat.id]) {
      console.log('Jawab is busy for', message.chat.id);
      return;
    }

    if (busyTimeout[message.chat.id]) {
      console.log('Timeout process for', message.chat.id);
      return;
    }

    busy[message.chat.id] = true;
    if (answer == "/jawab@" + botUsername || answer == '/jawab') {
      getGameId(message.chat.id).then(function(id) {
        gameId = id;
        return getGameObject(id);
      }).then(function(gameObject) {
        return checkPlayer(gameId, gameObject, message.from.id);
      }).then(function(sylabel) {
        return bot.sendMessage(message.chat.id, i18next.t('word starts with {{sylabel}}', {
          lng: getLanguage(message.chat.id),
          replace: {
            sylabel: sylabel.syl
          }
        }), {
          reply_to_message_id: message.message_id,
          reply_markup: JSON.stringify({
            force_reply: true,
            selective: true
          }),
          parse_mode: 'HTML'
        });
      }).then(function(reply) {
        bot.onReplyToMessage(reply.chat.id, reply.message_id, function(replyAnswer) {
          processAnswer(replyAnswer.chat.id, replyAnswer.from.id, replyAnswer.text);
        });
      }).catch(function(error) {
        busy[message.chat.id] = false;
        console.log("jawab error [" + message.chat.id + "]", error);
      });
    } else {
      processAnswer(message.chat.id, message.from.id, answer);
    };
  };

  var helpProcessor = function(message) {
    sendMessage(message.chat.id, i18next.t("help {{bot}}", {
      lng: getLanguage(message.chat.id),
      replace: {
        bot: "@" + botUsername
      }
    }), {
      parse_mode: 'HTML'
    });
  };

  var startGameProcessor = function(message) {

    if (message.chat.type === 'private') {
      registerRoom(message.chat);
    }
    var startingWord;
    if (busyStart[message.chat.id]) {
      console.log("Start is busy for", message.chat.id);
      return;
    }

    busyStart[message.chat.id] = true;
    checkMinimumPlayer(message.chat.id).then(function() {
      clearTimeout(counterBuat[message.chat.id]);
      bot.sendChatAction(message.chat.id, 'typing');
      return getGameId(message.chat.id).then(function(gameid) {
          return isAlive(gameid, message.from.id);
        }).then(function() {
          return getStartingWord(message.chat.id);
        }).then(function(word) {
          startingWord = word;
          return checkWord(message.chat.id, word);
        })
        .then(function(sylabel) {
          return initGame(message.chat.id, startingWord, sylabel)
            .then(function(gameId) {
              return getNextPlayer(message.chat.id);
            })
            .then(function(username) {
              console.log("Game started [" + message.chat.id + "]");
              botan.track(message, "startgame");
              gameRecord[message.chat.id][username] = gameRecord[message.chat.id][username] + 0 || 0;
              return getGameMode(message.chat.id).then(function(mode) {
                return sendMessage(message.chat.id, i18next.t('game started with {{word}} on {{category}}', {
                  lng: getLanguage(message.chat.id),
                  replace: {
                    word: startingWord,
                    category: mode.toLowerCase()
                  }
                }) + i18next.t('{{player}} answer for {{sylabel}} in {{timeout}}', {
                  lng: getLanguage(message.chat.id),
                  replace: {
                    player: username,
                    sylabel: sylabel,
                    timeout: timeoutSec
                  }
                }), {
                  parse_mode: 'HTML'
                })
              });
            }).then(function() {
              busyStart[message.chat.id] = false;
              busy[message.chat.id] = false;
              counterTime[message.chat.id.toString()] = Date.now();
              counter['time' + message.chat.id.toString()] = setTimeout(timeoutAction.bind(null, message.chat.id, sylabel), timeout);
            });
        });
    }).catch(function(error) {
      busyStart[message.chat.id] = false;
      console.log('Start Game error [' + message.chat.id + "]", error);
      if (error.code === 6) {
        sendMessage(message.chat.id, i18next.t('no player', {
          lng: getLanguage(message.chat.id)
        }), {
          parse_mode: 'HTML'
        });
      } else if (error.code === 4) {
        sendMessage(message.chat.id, i18next.t('no game', {
          lng: getLanguage(message.chat.id)
        }));
      } else if (error.code === 7) {
        console.log(SKIP_ERROR_MESSAGE, error);
        // sendMessage(message.chat.id, "Ouch! Game sudah dimulai. Ketik /buatgame untuk memulai game baru setelah selesai.");
      } else if (error.code === 9) {
        sendMessage(message.chat.id, i18next.t('{{player}} not yet join', {
          lng: getLanguage(message.chat.id),
          replace: {
            player: (message.from.username) ? '@' + message.from.username : message.from.first_name + message.from.last_name
          }
        }));
      }
    });
  };

  var joinProcessor = function(message) {

    // if (!message.from.username) {
    //     sendMessage(message.chat.id, i18next.t('fail to {{command}} {{player}} not having username', {
    //         lng: getLanguage(message.chat.id),
    //         replace: {
    //             command: 'join',
    //             player: message.from.first_name
    //         }
    //     }));
    // } else {
    getGameId(message.chat.id).then(function(gameId) {
      return addParticipant(gameId, message.from);
    }).then(function(alreadyJoin) {
      if (alreadyJoin) {
        sendMessage(message.chat.id, i18next.t('{{player}} already join', {
          lng: getLanguage(message.chat.id),
          replace: {
            player: (message.from.username) ? '@' + message.from.username : message.from.first_name + " " + message.from.last_name
          }
        }));
      } else {
        botan.track(message, "join");
        sendMessage(message.chat.id, i18next.t('{{player}} joined', {
          lng: getLanguage(message.chat.id),
          replace: {
            player: (message.from.username) ? '@' + message.from.username : message.from.first_name + " " + message.from.last_name
          }
        }));
      }
    }).catch(function(error) {
      console.log("join error", error);
      if (error.code === 4) {
        getGameMode(message.chat.id).then(function(mode) {
          var lang = 'en';
          if (mode == 'Indonesia') {
            lang = 'id';
          } else {
            lang = 'en'
          }
          sendMessage(message.chat.id, i18next.t("no game", {
            lng: lang
          }));
        });
      } else if (error.code === 7) {
        console.log(SKIP_ERROR_MESSAGE, error);
        // sendMessage(message.chat.id, "Ouch! Game sudah dimulai. Tunggu game selanjutnya untuk /join");
      }
    });
    // };
  };

  var inlineQueryProcessor = function(inlineQuery) {
    var query = inlineQuery.query,
      answerList = [];
    if (query.trim().length > 0) {
      indoDict.checkMeaning(query).then(function(parsedData) {
        var meaning = parsedData.list,
          meaningBody = parsedData.body;
        for (var k = 0; k < meaning.length; k++) {
          answerList.push({
            "type": 'article',
            "id": random.uuid4(),
            "title": meaning[k],
            input_message_content: {
              message_text: meaningBody[meaning[k]].join(" "),
              parse_mode: 'HTML'
            }
          });
        }
        bot.answerInlineQuery(inlineQuery.id, answerList).catch(function(error) {
          console.log("send inline error", error);
        });
      }).catch(function(error) {
        console.log(error);
        console.log("Inline not found");
      });
    }
  };

  var newChatParticipantProcessor = function(message) {
    var chatId = message.chat.id;
    if (message.new_chat_participant && message.new_chat_participant.username === botUsername) {
      registerRoom(message.chat);
      sendMessage(chatId, i18next.t("{{bot}} invited", {
        lng: 'en',
        replace: {
          bot: "@" + botUsername
        }
      }));
      botan.track(message, "invitation");
      return;
    }
  };

  var setRandomMode = function(chatId, randomMode) {
    myFirebaseRef.child("rooms").child(chatId).child('random').transaction(function(data) {
      return randomMode;
    }).then(function(randomData) {
      if (randomData.committed) {

        sendMessage(chatId, i18next.t('random mode now {{random}}', {
          lng: getLanguage(chatId),
          replace: {
            random: randomMode
          }
        }));
      }
    });
  };

  var setGameMode = function(chatId, gameMode) {
    myFirebaseRef.child("rooms").child(chatId).child('mode').transaction(function(data) {
      return gameMode;
    }).then(function(randomData) {
      if (randomData.committed) {
        getLanguagePromise(chatId).then(function(lang) {
          sendMessage(chatId, i18next.t('game mode now {{mode}}', {
            lng: getLanguage(chatId),
            replace: {
              mode: gameMode
            }
          }));
        });
      }
    });
  };


  var randomModeProcessor = function(chatId, randomMode) {
    getGameId(chatId).then(function(gameId) {
      return isGameNotStarted(gameId)
    }).then(function() {
      return setRandomMode(chatId, randomMode);
    }).catch(function(error) {
      console.log("random error", error);
      if (error.code == 7) {
        myFirebaseRef.child("rooms").child(chatId).child("random").once('value').then(function(randomSnapshot) {
          var currentRandomMode = randomSnapshot.val() || 'Random';
          sendMessage(chatId, i18next.t('current random mode is {{mode}}', {
            lng: getLanguage(chatId),
            replace: {
              mode: currentRandomMode
            }
          }));
        });
      } else if (error.code == 4) {
        setRandomMode(chatId, randomMode);
      };
    });
  };

  function getLanguage(chatId) {
    getLanguagePromise(chatId);
    if (!roomLanguage[chatId]) {
      return 'id';
    } else {
      return roomLanguage[chatId];
    }
  };

  function getLanguagePromise(chatId) {
    if (!roomLanguage[chatId]) {
      return getGameMode(chatId).then(function(mode) {
        if (mode == null || mode === 'Indonesia') {
          roomLanguage[chatId] = 'id';
          return Promise.resolve('id');
        } else {
          roomLanguage[chatId] = 'en';
          return Promise.resolve('en');
        }
      });
    } else {
      return Promise.resolve(roomLanguage[chatId]);
    }
  }

  var gameModeProcessor = function(chatId, gameMode) {
    getGameId(chatId).then(function(gameId) {
      return isGameNotStarted(gameId)
    }).then(function() {
      return setGameMode(chatId, gameMode);
    }).catch(function(error) {
      console.log("game mode error", error);
      if (error.code == 7) {
        myFirebaseRef.child("rooms").child(chatId).child("mode").once('value').then(function(randomSnapshot) {
          var currentRandomMode = randomSnapshot.val() || 'Indonesia',
            lang = 'id';
          if (currentRandomMode == 'Indonesia') {
            lang = 'id';
          } else {
            lang = 'en';
          }
          sendMessage(chatId, i18next.t('current game mode is {{mode}}', {
            lng: lang,
            replace: {
              mode: currentRandomMode
            }
          }));
        });
      } else if (error.code == 4) {
        setGameMode(chatId, gameMode);
      };
    });
  };

  var resetData = function() {
    return new Promise(function(resolve, reject) {
      var roomRef = myFirebaseRef.child("rooms"),
        ingameref = myFirebaseRef.child("ingame").set(false),
        wordsRef = myFirebaseRef.child('words').set(false),
        gamesRef = myFirebaseRef.child('games').set(false);
      ingameref.then(function() {
        return roomRef.once('value');
      }).then(function(roomsSnapshot) {
        var resetRooms = [];
        // roomsSnapshot.forEach(function(room) {
        //   resetRooms.push(getLanguage(room.key).then(function(lang) {
        //     getLanguage(chatId)[room.key] = lang;
        //     return versionCheck(room.key);
        //   }));
        //   // sendMessage(room.key, 'Hi! @' + botUsername + ' kembali daring!\n\nSelamat Bermain Sambung Kata ya...').catch(function (error) {
        //   //     console.log("Reset Data error", error);
        //   //     if (error.error_code === 400) {
        //   //         roomRef.child(room.key).remove();
        //   //     }
        //   // });
        // });
        Promise.all(resetRooms).then(function() {
          console.log("Reset data is success");
          newVersion = false;
          resolve();
        });
      });
    });
  }

  var newVersion = false;
  var firstRun = true;

  var versionCheck = function(roomId) {
    var currentVersion = '6',
      greetingMessage = i18next.t('{{bot}} back online', {
        lng: 'en',
        replace: {
          bot: '@' + botUsername
        }
      }) + '\n\n',
      currentChangelog = 'Changelog :\n' +
      'Game mode and Random Mode updated!\n' +
      'To set Game mode or Random Mode, please use the following command : \n\n' +
      '/setshufflemodenormal to set Normal shuffle mode\n' +
      '/setshufflemoderandom to set Random shuffle mode\n' +
      '/setgamemodeindonesia to set Indonesia as game mode\n' +
      '/setgamemodeverb to set Verb as game mode\n' +
      '/setgamemodenoun to set Noun as game mode\n' +
      '/setgamemodeadjective to set Adjective as game mode\n\n' +
      'p.s : ' +
      '\nYou don\'t need to memorize all the command. Check / button on your bottom right to see all available command.' +
      '\nLanguage will changed to English when game mode is not Indonesia' +
      '\nModes could be changed as long as game is not started';

    if (firstRun) {
      var currentVersionRef = myFirebaseRef.child('version').child(currentVersion);
      // Read current Changelog
      return currentVersionRef.once('value').then(function(changelogSnapshot) {
        firstRun = false;
        if (changelogSnapshot.val() == null) {
          //  New Version detected
          return currentVersionRef.transaction(function(value) {
            return currentChangelog
          }).then(function(changelogTransaction) {
            if (changelogTransaction.committed) {
              newVersion = true;
              // console.log("send changelog to " + roomId);
              return sendMessage(roomId, greetingMessage + currentChangelog);
            }
          });
        }
      });
    } else {
      if (newVersion) {
        // console.log("send changelog to " + roomId);
        return sendMessage(roomId, greetingMessage + currentChangelog);
      }
    }
  }

  function howToProcessor(message) {
    getLanguagePromise(message.chat.id).then(function(language) {
      return sendMessage(message.chat.id, i18next.t('howto', {
        lng: language
      }));
    });
  }

  function showHighScoreProcessor(message) {
    getLanguagePromise(message.chat.id).then(function(language) {
      myFirebaseRef.child('rooms').child(message.chat.id).child('scores').orderByChild('score').limitToLast(5).once('value').then(function(scoreSnapshot) {
        var highScores = [];
        scoreSnapshot.forEach(function(data) {
          var name = (data.val().username) ? '@' + data.val().username : data.val().first_name + " " + data.val().last_name,
            score = data.val().score;
          highScores.unshift(name + ' : ' + score);
        });
        return sendMessage(message.chat.id, 'Group Leaderboard\n\n' + highScores.join('\n').toString());
      })
    });
  }

  function deleteRoomProcessor(roomId) {
    myFirebaseRef.child('rooms').child(roomId).remove().then(function() {
      return myFirebaseRef.child('ingame').child(roomId).remove();
    }).then(function() {
      console.log('Removed from game', roomId);
    });
  }

  function incomingMessageProcessor(message) {
    chatBottleHandler.incomingMessageProcessor(message);
  };

  return {
    buatGameProcessor: buatGameProcessor,
    jawabProcessor: jawabProcessor,
    helpProcessor: helpProcessor,
    startGameProcessor: startGameProcessor,
    joinProcessor: joinProcessor,
    inlineQueryProcessor: inlineQueryProcessor,
    newChatParticipantProcessor: newChatParticipantProcessor,
    randomModeProcessor: randomModeProcessor,
    gameModeProcessor: gameModeProcessor,
    isAliveProcessor: isAliveProcessor,
    howToProcessor: howToProcessor,
    resetData: resetData,
    showHighScoreProcessor: showHighScoreProcessor,
    deleteRoomProcessor: deleteRoomProcessor,
    incomingMessageProcessor: incomingMessageProcessor
  };
};

module.exports = processor;
