//  Dependency of dictionary request
var Promise = require('bluebird');
var osmosis = Promise.promisifyAll(require('osmosis'));
var myFirebaseRef = require('./SambungkatabotFirebase.js');
var noodle = require('noodlejs');
var pg = require('pg');
Promise.promisifyAll(pg);
var fs = require('fs');

var config = {
  host : process.env.DATABASE_HOST,
  ssl : true,
  max: 15, // max number of clients in the pool
  idleTimeoutMillis: 30000, // how long a client is allowed to remain idle before being closed
};

// pg.defaults.ssl = true;

var pool = new pg.Pool(config)

var dictionaryIndonesia = (function () {
    'use strict';

    var detailsQuery = function (searchWord, currentWord, wordList) {
        return osmosis.post(process.env.KBBI_URL, {
                "OPKODE": 1,
                "PARAM": searchWord,
                "PERINTAH": "Tampilkan",
                "KATA": currentWord,
                "DFTKATA": wordList.join(';')
            }).find('p b')
            .set('arti');
    }

    var localSylabelLookup = function (word) {
        return new Promise(function (resolve, reject) {
            pool.connect(function (err, client, done) {
                let resultSet = [],
                searchSQL = 'SELECT WORD FROM TBLKATA WHERE WORD = \'' + word + '\' OR WORD ~* \'^'+word+'(\\s*).[1-9].$\'';

                var searchQuery = client.query(searchSQL);
                searchQuery.on('row', function (row) {
                    resultSet.push(row.word);
                });

                searchQuery.on('error', function (error) {
                    reject("local search fail");
                    console.log("local called find start with error " + error);
                    // resultSet.push(row);
                });

                searchQuery.on('end', function () {
                    let searchThreads = [],
                        syl = [],
                        sylabelResult;
                    for (var i = 0; i < resultSet.length; i++) {
                        searchThreads.push((function (wordTarget) {
                            'use strict';
                            return new Promise(function (resolve, reject) {
                                var detailsQuery = client.query('SELECT HTML FROM TBLKATA WHERE WORD = \'' + wordTarget + '\'');

                                detailsQuery.on('row', function (row) {
                                    osmosis.parse(row.html.replace(/\r/g, "").replace(/\n/g, "").trim())
                                        .find('p b')
                                        .set('arti')
                                        .data(function (result) {
                                            var current = result.arti.replace(/[0-9]/g, '').trim(),
                                                subWord = current.split(',');
                                            for (var j = 0; j < subWord.length; j++) {
                                                if (subWord[j].indexOf('·') > -1 || subWord[j].toLowerCase() == word.toLowerCase()) {
                                                    if (syl.indexOf(subWord[j]) == -1) {
                                                        syl.push(subWord[j]);
                                                    }
                                                }
                                            }
                                        }).log(console.log)
                                        .error(console.error)
                                        .debug(console.log);
                                });

                                detailsQuery.on('error', function (error) {
                                    reject("Local search details error");
                                    console.log("details error", error);
                                });

                                detailsQuery.on('end', function () {
                                    resolve();
                                });
                            });
                        }(resultSet[i])));
                    }

                    Promise.all(searchThreads).then(function () {
                        for (var k = 0; k < syl.length && !sylabelResult; k++) {
                            var currentWord = syl[k],
                                realWord = currentWord.split('·').join('').trim(),
                                sylabelArray = [];
                            if (realWord.toLowerCase() == word.toLowerCase()) {
                                sylabelArray = currentWord.split('·');
                                sylabelResult = sylabelArray.pop();
                            }
                        }
                        if (sylabelResult) {
                            resolve(sylabelResult);
                        } else {
                            reject({
                                code: 2,
                                message: "Word not found"
                            });
                        }
                        done();
                    });

                });
            });
        });
    }

    var sylabelLookup = function (word, wordList) {
        return new Promise(function (resolve, reject) {
            var searchWord = [],
                syl = [],
                sylabelResult;
            for (var i = 0; i < wordList.length; i++) {
                searchWord.push((function (currentArti) {
                    return detailsQuery(word, currentArti, wordList)
                        .data(function (result) {
                            var current = result.arti.replace(/[0-9]/g, '').trim(),
                                subWord = current.split(',');
                            for (var j = 0; j < subWord.length; j++) {
                                if (subWord[j].indexOf('·') > -1 || subWord[j].toLowerCase() == word.toLowerCase()) {
                                    if (syl.indexOf(subWord[j]) == -1) {
                                        syl.push(subWord[j]);
                                    }
                                }
                            }
                        }).error(console.log);

                })(wordList[i]));
            }
            Promise.all(searchWord).then(function () {
                for (var k = 0; k < syl.length && !sylabelResult; k++) {
                    var currentWord = syl[k],
                        realWord = currentWord.split('·').join('').trim(),
                        sylabelArray = [];
                    if (realWord.toLowerCase() == word.toLowerCase()) {
                        sylabelArray = currentWord.split('·');
                        sylabelResult = sylabelArray.pop();
                    }
                }
                if (sylabelResult) {
                    resolve(sylabelResult);
                } else {
                    reject({
                        code: 2,
                        message: "Word not found"
                    });
                }
            });
        });
    }

    var searchQuery = function (word) {
        return osmosis.post(process.env.KBBI_URL, {
                "OPKODE": 1,
                "PARAM": word,
                "PERINTAH": "Cari"
            }).find('.isipusba a')
            .set('arti');
    }

    var wordLookup = function (word) {
      console.log('online kbbi for ',word);
        return new Promise(function (resolve, reject) {
            var artiList = [];
            searchQuery(word).data(function (result) {
                artiList.push(result.arti);
            }).done(function () {
                if (artiList.length == 0) {
                    reject({
                        code: 2,
                        message: "Word not found"
                    });
                    return;
                } else {
                    resolve(artiList);
                }
            }).error(function (error) {
                console.log("Online Lookup error", error);
                reject({
                    code: 2,
                    message: "Word not found"
                });
            }).debug(console.log).error(console.log);
        });
    }

    var localMeaningLookup = function (word) {
        return new Promise(function (resolve, reject) {
            pool.connect(function (err, client, done) {
              let resultSet = [],
                searchSQL = 'SELECT WORD FROM TBLKATA WHERE WORD = \'' + word + '\' OR WORD ~* \'^'+word+'(\\s*).[1-9].$\'';


              var searchQuery = client.query(searchSQL);
              searchQuery.on('row', function (row) {
                  resultSet.push(row.word);
              });

              searchQuery.on('error', function (error) {
                  reject("local search fail");
                  console.log("local called find start with error " + error);
                  // resultSet.push(row);
              });

              searchQuery.on('end', function () {

                let meaningThread = [],
                  artiBody = [];

                for (var i = 0; i < resultSet.length; i++) {
                  meaningThread.push((function(currentWord) {
                    'use strict';
                    return new Promise(function (resolve, reject) {
                        var detailsQuery = client.query('SELECT HTML FROM TBLKATA WHERE WORD = \'' + currentWord + '\'');
                        detailsQuery.on('row', function(row) {
                          osmosis.parse(row.html).find('p')
                          .set('arti').data(function (result) {
                              var words = result.arti.replace(/(?:\\[r])+/g, "").replace(/(?:\\[n])+/g, " \n ").split(" "),
                                  isKind, startExample, example;
                              for (var pos = 0; pos < words.length; pos++) {
                                  if (pos == 0 || words[pos].indexOf('·') > -1) {
                                      words[pos] = "<b>" + words[pos] + "</b>";
                                      isKind = true;
                                  } else if (!isNaN(words[pos]) && !example) {
                                      words[pos] = "<b>" + words[pos] + "</b>";
                                  } else if (isKind) {
                                      words[pos] = "<i>" + words[pos] + "</i>";
                                      isKind = false;
                                  } else if (words[pos].endsWith(":")) {
                                      startExample = true;
                                      example = true;
                                  } else if (startExample) {
                                      words[pos] = "<i>" + words[pos];
                                      startExample = false;
                                  } else if (words[pos].endsWith(";") && example) {
                                      words[pos] = words[pos] + "</i>";
                                      example = false;
                                  } else if (pos == words.length - 1) {
                                      if (example) {
                                          words[pos] = words[pos] + "</i>";
                                          example = false;
                                      }
                                  }
                              }
                              artiBody[currentWord] = words;
                          }).log(console.log)
                          .error(console.error)
                          .debug(console.log);
                        });

                        detailsQuery.on('end', function () {
                            resolve();
                        });
                      });
                  }(resultSet[i])));
                }
                Promise.all(meaningThread).then(function () {
                    done();
                    resolve({
                        "list": resultSet,
                        "body": artiBody
                    });
                });
              });
            });
        });
    }

    var meaningLookup = function (word, wordList) {

        return new Promise(function (resolve, reject) {
            var answerList = [],
                requests = [],
                artiBody = {};
            for (var n = 0; n < wordList.length; n++) {
                requests.push((function (currentWord) {
                    return osmosis.post(process.env.KBBI_URL, {
                            "OPKODE": 1,
                            "PARAM": word,
                            "PERINTAH": "Tampilkan",
                            "KATA": currentWord,
                            "DFTKATA": wordList.join(';')
                        }).find('p')
                        .set('arti').data(function (result) {
                            var words = result.arti.replace(/\r/g, "").replace(/\n/g, " \n ").split(" "),
                                isKind, startExample, example;
                            for (var pos = 0; pos < words.length; pos++) {
                                if (pos == 0 || words[pos].indexOf('·') > -1) {
                                    words[pos] = "<b>" + words[pos] + "</b>";
                                    isKind = true;
                                } else if (!isNaN(words[pos]) && !example) {
                                    words[pos] = "<b>" + words[pos] + "</b>";
                                } else if (isKind) {
                                    words[pos] = "<i>" + words[pos] + "</i>";
                                    isKind = false;
                                } else if (words[pos].endsWith(":")) {
                                    startExample = true;
                                    example = true;
                                } else if (startExample) {
                                    words[pos] = "<i>" + words[pos];
                                    startExample = false;
                                } else if (words[pos].endsWith(";") && example) {
                                    words[pos] = words[pos] + "</i>";
                                    example = false;
                                } else if (pos == words.length - 1) {
                                    if (example) {
                                        words[pos] = words[pos] + "</i>";
                                        example = false;
                                    }
                                }
                            }
                            artiBody[currentWord] = words;
                        }).error(console.log);
                })(wordList[n]));
            }
            Promise.all(requests).then(function () {
                resolve({
                    "list": wordList,
                    "body": artiBody
                });
            });
        });
    }

    var checkWord = function (searchWord) {
        return localSylabelLookup(searchWord).catch(function (error) {
          console.log(error);
            return wordLookup(searchWord).then(function (wordList) {
                return sylabelLookup(searchWord, wordList);
            });
        });
    }

    var checkMeaning = function (searchWord) {
      return localMeaningLookup(searchWord).catch(function() {
        return wordLookup(searchWord).then(function (wordList) {
            return meaningLookup(searchWord, wordList);
        });
      });
    }

    function getStartingWord() {
        return new Promise(function (resolve, reject) {
          pool.connect(function (err, client, done) {
            let resultSet, searchSQL = 'SELECT WORD FROM TBLKATA WHERE WORD NOT LIKE \'%-%\' AND WORD NOT LIKE \'%?%\' ORDER BY RANDOM() LIMIT 1';


            var searchQuery = client.query(searchSQL);
            searchQuery.on('row', function (row) {
                resultSet = row.word;
            });

            searchQuery.on('error', function (error) {
                reject("local search fail");
                console.log("local called find start with error " + error);
                // resultSet.push(row);
            });

            searchQuery.on('end', function () {
              console.log('starting word', resultSet);
              if(resultSet.indexOf(' ') > -1){
                resolve(resultSet.slice(0, resultSet.indexOf(' ')));
              } else {
                resolve(resultSet);
              }
              done();
            });
          });
        });
    };

    return {
        checkWord : checkWord,
        checkMeaning : checkMeaning,
        getStartingWord : getStartingWord
    }
}());
module.exports = dictionaryIndonesia;
